#!/usr/bin/env ruby
# Fuzz Buzz Code
# fourth solution i want add the test for the fizzbuzz app
# continue being boring this test, Good By
# work too

require "minitest/autorun"

class Fixnum
  def to_fizzbuzz
    ((self%3==0)||(self%5==0))?((self%3==0)?"fizz":"")+ ((self%5==0)?"buzz":""): self
  end
end

(1..100).each {
  |n| puts n.to_fizzbuzz
}

class TestFizzBuzz < Minitest::Test
  def test_third_multiple
    assert_equal "fizz", 33.to_fizzbuzz
  end
  def test_prime
    assert_equal 59, 59.to_fizzbuzz
  end
  def test_fifth_multiple
    assert_equal "buzz", 35.to_fizzbuzz
  end
  def test_third_fifth_multiple
    assert_equal "fizzbuzz", 30.to_fizzbuzz
  end

end
